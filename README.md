Rigel
=====

A Drupal [Bootstrap 5](https://drupal.org/project/bootstrap5) based sub-theme.
Designed for [Cloud](https://drupal.org/project/cloud|cloud) module

Setup for CSS development
=========================

To make css changes, work on the SCSS files and use the following instructions
to compile the scss files into css.  After compilation, the css file runs
through `stylelint` to fix any code formatting issues.

1. Install node.js using Homebrew, or other preferred installation method.
Homebrew command is `brew install node`
2. In the `rigel` top-level theme directory, run `npm install`.
3. Run `npm install -g grunt-cli` to install Grunt Cli.
4. To watch for scss file changes, in the `rigel` top-level theme directory,
run `grunt -v`.
5. Make a change to a scss file. The `main.css` should rebuild.
6. Before checking the code in, run `prettier` to make sure the code
is formatted by running the following `npx prettier --config .prettierrc.json --write "assets/css/main.css"`
This is still a manual process.  Prettier did not work properly with Grunt.

To check for stylelint errors, run the following command from Drupal's core
directory
`stylelint --formatter verbose --config ./.stylelintrc.json ../themes/contrib/rigel/assets/css/main.css`

Development notes
=================
-
- `stylelint` and `stylelint-config-drupal` are used to format the compiled
`main.css` file.  Drupal core uses `stylelint` to format css files:
https://www.drupal.org/project/drupal/issues/2865971
- Information about `stylelint-config-drupal` are found here:
https://npm.io/package/stylelint-config-drupal

License
=======

Cloud Orchestrator is released under the terms of the [GPLv2 license](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html#SEC1).

Maintainers
===========

- `Yas Naoi (yas)` (https://drupal.org/u/yas)
- `Baldwin Louie (baldwinlouie)` (https://www.drupal.org/u/baldwinlouie)
