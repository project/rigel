services:
  php:
    image: q0rban/tugboat-drupal:9.4
    privileged: true
    default: true
    http: false
    depends: memcached
    # depends: mysql
    commands:
      init: |
        set -eux

        curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
        apt-get update > /dev/null 2>&1 && \
        apt-get install -y --no-install-recommends gcc g++ make nodejs yarnpkg \
          cron \
          git \
          libfreetype6-dev \
          libjpeg-dev \
          libmemcached-dev \
          libpng-dev \
          libzip-dev \
          sqlite3 \
          unzip \
          zip \
          zlib1g > /dev/null 2>&1

        # Avoid an error: "Allowed memory size of 134217728 bytes exhausted"
        export PHP_INI='/usr/local/etc/php/conf.d/my-php.ini'
        echo 'memory_limit = -1' >> "${PHP_INI}"

        # Set up the max execution time.
        echo 'max_execution_time = 600' >> "${PHP_INI}"

        # Set up the max input time.
        echo 'max_input_time = 600' >> "${PHP_INI}"

        # Set up the max input vars.
        echo 'max_input_vars = 100000' >> "${PHP_INI}"

        # Check out a branch using the unique Tugboat ID for this repository, to
        # ensure we don't clobber an existing branch.
        git checkout -b "${TUGBOAT_REPO_ID}"

        # Install Memcached libraries.
        git clone https://github.com/php-memcached-dev/php-memcached /usr/src/php/ext/memcached
        cd /usr/src/php/ext/memcached
        docker-php-ext-install memcached > /dev/null 2>&1
        echo 'memcache.hash_strategy=consistent' >> /usr/local/etc/php/conf.d/docker-php-ext-memcached.ini

        # Install APCu libraries.
        git clone https://github.com/krakjoe/apcu /usr/src/php/ext/apcu
        cd /usr/src/php/ext/apcu
        docker-php-ext-install apcu > /dev/null 2>&1

        # Install OpCache.
        docker-php-ext-install -j "$(nproc)" opcache > /dev/null 2>&1

        # Install uploadprogress.
        pecl install uploadprogress > /dev/null 2>&1
        docker-php-ext-enable uploadprogress > /dev/null 2>&1

        # Install graphic libraries.
        docker-php-ext-configure gd \
          --with-freetype \
          --with-jpeg > /dev/null 2>&1
        docker-php-ext-install -j "$(nproc)" \
          gd \
          zip > /dev/null 2>&1

        # Composer is hungry. You need a Tugboat project with a pretty sizeable
        # chunk of memory.
        export COMPOSER_MEMORY_LIMIT=-1

        # This is an environment variable we added in the Dockerfile that
        # provides the path to Drupal composer root (not the web root).
        cd "${DRUPAL_COMPOSER_ROOT}"

        # We configure the Drupal project to use the checkout of the module as a
        # Composer package repository.
        composer config repositories.tugboat vcs "${TUGBOAT_ROOT}"

        # Allow "minimum-stability": "dev" and "prefer-stable": true
        composer config minimum-stability dev
        composer config prefer-stable true

      update: |
        set -eux

        # Now we can require docomoinnovations/cloud_orchestrator
        cd /var/www && composer create-project docomoinnovations/cloud_orchestrator:5.x-dev cloud_orchestrator

        # Re-write the pre-defined "${DRUPAL_COMPOSER_ROOT}" directory.
        export DRUPAL_COMPOSER_ROOT='/var/www/cloud_orchestrator'

        # This is an environment variable we added in the Dockerfile that
        # provides the path to Drupal composer root (not the web root).
        cd "${DRUPAL_COMPOSER_ROOT}"

        # We configure the Drupal project to use the checkout of the module as a
        # Composer package repository.
        composer config repositories.tugboat vcs "${TUGBOAT_ROOT}"

        # Allow "minimum-stability": "dev" and "prefer-stable": true
        composer config minimum-stability dev
        composer config prefer-stable true

        # Re-write the pre-defined "${DRUPAL_DOCROOT}" directory.
        rm -fr "${DRUPAL_DOCROOT}"
        ln -s "${DRUPAL_COMPOSER_ROOT}/docroot" "${DRUPAL_DOCROOT}"
        export DRUPAL_DOCROOT="${DRUPAL_COMPOSER_ROOT}/docroot"

        # Cheat the dependency that drupal/cloud:5.x-dev requires rigel:^5
        sed -i -e "s|drupal/rigel\": \"\^.*\"|drupal/rigel\": \"dev-${TUGBOAT_REPO_ID}\"|g" \
          "${DRUPAL_COMPOSER_ROOT}/composer.lock"

        # Now we can require drupal/rigel, specifying the branch name we created
        # above that uses the "${TUGBOAT_REPO_ID}" environment variable.
        composer require "drupal/rigel:dev-${TUGBOAT_REPO_ID}"

        # Copy default.settings.php to settings.php
        export SETTINGS_FILE="${DRUPAL_DOCROOT}/sites/default/settings.php"
        cp "${DRUPAL_DOCROOT}/sites/default/default.settings.php" "${SETTINGS_FILE}"
        chgrp -R www-data "${SETTINGS_FILE}"
        chmod -R g+w "${SETTINGS_FILE}"

        # $settings['file_private_path'] in settings.php must be written before
        # drush site:install (si).
        export FILES_DIR='sites/default/files'
        export PRIVATE_DIR="${FILES_DIR}/private"
        echo "\$settings['file_private_path'] = '${PRIVATE_DIR}';" >> "${SETTINGS_FILE}"

        # Set up $settings['trusted_host_patterns'] in settings.php
        echo "\$settings['trusted_host_patterns'] = ['^.+\.(tugboat\.qa|tugboatqa\.com)$'];" >> "${SETTINGS_FILE}"

        # Set up a drush command.
        ln -s "${DRUPAL_COMPOSER_ROOT}/vendor/bin/drush" /usr/local/bin/

        # Install Drupal on the site.
        # Use --db-url=mysql://tugboat:tugboat@mysql:3306/tugboat for mysql connection.
        cd "${DRUPAL_DOCROOT}"
        drush -y si \
          --db-url="sqlite://${FILES_DIR}/db.sqlite" \
          --site-name="Cloud Orchestrator live preview for '${TUGBOAT_PREVIEW_NAME}'" \
          --account-pass='admin' \
          cloud_orchestrator \
          cloud_orchestrator_module_configure_form.cloud_service_'providers.openstack=openstack' \
          cloud_orchestrator_module_configure_form.cloud_service_'providers.vmware=vmware' \
          cloud_orchestrator_module_configure_form.cloud_service_'providers.terraform=terraform'

        # Enable the additional modules.
        drush -y en cloud_dashboard
        drush -y en entity_export_csv
        drush -y en memcache memcache_admin
        drush -y en simple_oauth

        # Set a images/cloud/icons directory.
        mkdir -p "${DRUPAL_DOCROOT}/${FILES_DIR}/images/cloud/icons"

        # Set a private directory.
        mkdir -p "${PRIVATE_DIR}"

        # Set up a files directory.
        mkdir -p "${DRUPAL_DOCROOT}/${FILES_DIR}"
        chown -R www-data:www-data "${DRUPAL_DOCROOT}/${FILES_DIR}"
        chmod -R 2775 "${DRUPAL_DOCROOT}/${FILES_DIR}"
        chmod -R g+w "${DRUPAL_DOCROOT}/${FILES_DIR}"

        # The settings must be written after enabling the memcache module.
        tee -a "${SETTINGS_FILE}" > /dev/null << EOF
        \$settings['memcache']['servers'] = ['memcached:11211' => 'default'];
        \$settings['memcache']['bins'] = ['default' => 'default'];
        \$settings['memcache']['key_prefix'] = 'cloud_orchestrator';
        \$settings['memcache']['options'] = [
          Memcached::OPT_DISTRIBUTION => Memcached::DISTRIBUTION_CONSISTENT,
        ];
        \$settings['cache']['default'] = 'cache.backend.memcache';
        EOF
        chmod 444 "${SETTINGS_FILE}"

        # Setup crontab.
        DRUSH_QUEUE_RUN_SCRIPT="${DRUPAL_DOCROOT}/modules/contrib/cloud/scripts/drush_queue_run.sh"
        chmod +x "${DRUSH_QUEUE_RUN_SCRIPT}"
        tee /etc/crontab > /dev/null <<EOF
        SHELL=/bin/sh
        PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
        MAILTO=''

        */1 * * * * www-data cd '${DRUPAL_DOCROOT}'; drush cron > /dev/null 2>&1
        */15 * * * * www-data cd '${DRUPAL_DOCROOT}'; '${DRUSH_QUEUE_RUN_SCRIPT}' > /dev/null 2>&1
        EOF

      build: |
        set -eux

        # Re-write the pre-defined environment variables.
        export DRUPAL_COMPOSER_ROOT='/var/www/cloud_orchestrator'
        export DRUPAL_DOCROOT="${DRUPAL_COMPOSER_ROOT}/docroot"

        # Delete and re-check out this branch in case this is built from a Base Preview.
        git checkout 5.x && git branch -D "${TUGBOAT_REPO_ID}" && git checkout -b "${TUGBOAT_REPO_ID}" || true
        export COMPOSER_MEMORY_LIMIT=-1
        cd "${DRUPAL_COMPOSER_ROOT}"
        composer install --optimize-autoloader
        # Do NOT uncomment the following line since the following error occurs:
        # "Could not authenticate against github.com" for github.com rate limits.
        # composer update docomoinnovations/cloud_orchestrator --with-all-dependencies
        # composer update "drupal/rigel:dev-${TUGBOAT_REPO_ID}"

        # Build and install cloud_dashboard.
        export NODE_PATH='/usr/lib/nodejs:/usr/share/nodejs'
        cd "${DRUPAL_DOCROOT}/modules/contrib/cloud/modules/cloud_dashboard/cloud_dashboard"
        npm install -g npm@8.3.0
        npm install babel-runtime
        ln -s /usr/bin/yarnpkg /usr/bin/yarn
        ./build.sh

        # Update databases and clear cache.
        cd "${DRUPAL_DOCROOT}"
        drush -y updb
        drush cr

  # mysql:
  #   image: tugboatqa/mariadb

  memcached:
    image: tugboatqa/memcached
