/**
 * @file
 * Grunt file for compiling SCSS to CSS.
 */
module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // Tasks
    'dart-sass': {
      target: {
        options: {
          sourceMap: true
        },
        files: [
          {
            expand: true,
            cwd: 'assets/scss/',
            src: ['main.scss'],
            dest: 'assets/css',
            ext: '.css'
          }
        ]
      }
    },
    shell: {
      prettier: {
        command:
          'npx prettier --config .prettierrc.json --write "assets/css/**/*.css"'
      }
    },
    watch: {
      // Compile everything into one task with Watch Plugin
      css: {
        files: 'assets/scss/*.scss',
        tasks: ['dart-sass', 'shell:prettier']
      }
    }
  });
  grunt.loadNpmTasks('grunt-dart-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-shell');

  grunt.registerTask('default', ['watch']);
};
