<?php

namespace Drupal\rigel;

/**
 * The primary class for the Drupal Bootstrap base theme.
 *
 * Provides many helper methods.
 *
 * @ingroup utility
 */
class Bootstrap {

  /**
   * Matches a Bootstrap class based on a string value.
   *
   * @param string $label
   *   The default class to return if no match is found.
   * @param string $default
   *   The default class to return if no match is found.
   *
   * @return string
   *   The Bootstrap class matched against the value of $haystack or $default
   *   if no match could be made.
   */
  public static function cssClassFromString($label, $default = '') {
    static $lang;
    if (!isset($lang)) {
      $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    }
    // Allow sub-themes to alter this array of patterns.
    /** @var \Drupal\Core\Theme\ThemeManager $theme_manager */
    $data = [
      // Text that match these specific strings are checked first.
      'matches' => [
        // Primary class.
        t('Download feature')->render() => 'primary',

        // Success class.
        t('Add effect')->render() => 'success',
        t('Add and configure')->render()  => 'success',
        t('Save configuration')->render() => 'success',
        t('Install and set as default')->render() => 'success',

        // Info class.
        t('Save and add')->render() => 'info',
        t('Add another item')->render() => 'plus',
        t('Update style')->render() => 'info',
        t('Disassociate')->render() => 'remove',
        t('Associate')->render() => 'ok',
        t('Add')->render() => 'plus',
        t('Start')->render() => 'play',
        t('Reboot')->render() => 'repeat',
        t('Stop')->render() => 'stop',
        t('Terminate')->render() => 'remove',
        t('Delete')->render() => 'trash',
        t('Create')->render() => 'plus',
        t('Copy')->render() => 'duplicate',
        t('List')->render() => 'th-list',
        t('Launch')->render() => 'play',
        t('Refresh All')->render() => 'refresh',
        t('Refresh')->render() => 'repeat',
        t('Log out')->render() => 'log-out',
        t('Apply')->render() => 'ok',
        t('Attach')->render() => 'tag',
        t('Detach')->render() => 'tags',
        t('Accept')->render() => 'ok',
        t('Approve')->render() => 'ok',
        t('Review')->render() => 'comment',
        t('Devel')->render() => 'devel',

        t('Manage')->render() => 'cog',
        t('Configure')->render() => 'cog',
        t('Settings')->render() => 'cog',
        t('Download')->render() => 'download',
        t('Export')->render() => 'export',
        t('Filter')->render() => 'filter',
        t('Import')->render() => 'download-alt',
        t('Save')->render() => 'ok',
        t('Update')->render() => 'ok',
        t('Edit')->render() => 'wrench',
        t('Uninstall')->render() => 'trash',
        t('Install')->render() => 'plus',
        t('Write')->render() => 'plus',
        t('Cancel')->render() => 'remove',
        t('Remove')->render() => 'trash',
        t('Reset')->render() => 'remove-circle',
        t('Search')->render() => 'search',
        t('Upload')->render() => 'upload',
        t('Preview')->render() => 'eye-open',
        t('Log in')->render() => 'log-in',
        t('Console Output')->render() => 'modal-window',
      ],

      // Text containing these words anywhere in the string are checked last.
      'contains' => [
        // Primary class.
        t('Confirm')->render() => 'primary',
        t('Filter')->render() => 'primary',
        t('Log in')->render() => 'primary',
        // t('Submit')->render() => 'primary',.
        t('Search')->render() => 'primary',
        t('Settings')->render() => 'primary',
        t('Upload')->render() => 'primary',
        // t('Refresh')->render() => 'primary',
        // Danger class.
        t('Delete')->render() => 'danger',
        t('Remove')->render() => 'danger',
        t('Reset')->render() => 'danger',
        t('Uninstall')->render() => 'danger',

        // Success class.
        t('Disassociate Address')->render() => 'remove',
        t('Add')->render() => 'success',
        t('Create')->render() => 'success',
        t('Install')->render() => 'success',
        t('Save')->render() => 'success',
        t('Write')->render() => 'success',

        // Warning class.
        t('Export')->render() => 'warning',
        t('Import')->render() => 'warning',
        t('Restore')->render() => 'warning',
        t('Rebuild')->render() => 'warning',

        // Info class.
        t('Apply')->render() => 'info',
        t('Update')->render() => 'info',
        t('Disassociate')->render() => 'remove',
        t('Disassociate Address')->render() => 'remove',
        t('Associate')->render() => 'ok',
        t('Add')->render() => 'plus',
        t('Start')->render() => 'play',
        t('Reboot')->render() => 'repeat',
        t('Stop')->render() => 'stop',
        t('Terminate')->render() => 'remove',
        t('Delete')->render() => 'trash',
        t('Create')->render() => 'plus',
        t('Copy')->render() => 'duplicate',
        t('List')->render() => 'th-list',
        t('Launch')->render() => 'play',
        t('Refresh All')->render() => 'refresh',
        t('Refresh')->render() => 'repeat',
        t('Log out')->render() => 'log-out',
        t('Apply')->render() => 'ok',
        t('Attach')->render() => 'tag',
        t('Detach')->render() => 'tags',
        t('Accept')->render() => 'ok',
        t('Approve')->render() => 'ok',
        t('Review')->render() => 'comment',
        t('Devel')->render() => 'devel',

        t('Manage')->render() => 'cog',
        t('Configure')->render() => 'cog',
        t('Settings')->render() => 'cog',
        t('Download')->render() => 'download',
        t('Export')->render() => 'export',
        t('Filter')->render() => 'filter',
        t('Import')->render() => 'download-alt',
        t('Save')->render() => 'ok',
        t('Update')->render() => 'ok',
        t('Edit')->render() => 'wrench',
        t('Uninstall')->render() => 'trash',
        t('Install')->render() => 'plus',
        t('Write')->render() => 'plus',
        t('Cancel')->render() => 'remove',
        t('Remove')->render() => 'trash',
        t('Reset')->render() => 'remove-circle',
        t('Search')->render() => 'search',
        t('Upload')->render() => 'upload',
        t('Preview')->render() => 'eye-open',
        t('Log in')->render() => 'log-in',
      ],
    ];
    $theme_manager = \Drupal::service('theme.manager');
    $theme_manager->alter('bootstrap_colorize_text', $data);

    // Iterate over the array.
    foreach ($data as $pattern => $strings) {
      foreach ($strings as $text => $class) {
        switch ($pattern) {
          case 'matches':
            if ($label === $text) {
              return $class;
            }
            break;

          case 'contains':
            if (strpos($label, $text) !== FALSE) {
              return $class;
            }
            break;
        }
      }
    }

    // Return the default if nothing was matched.
    return $default;
  }

}
