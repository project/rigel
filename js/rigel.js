/**
 * @file
 * Text resize options display.
 */
(function ($) {
  'use strict';
  Drupal.behaviors.fontBehavior = {
    attach: function () {
      let font_family =
        drupalSettings.rigel.font_family === null
          ? 'Lucida Grande'
          : drupalSettings.rigel.font_family;

      $('#edit-rigel')
        .find('strong')
        .siblings('span')
        .html(
          'Theme: ' +
            'Dark' +
            '<br /> ' +
            'Font family: ' +
            font_family +
            '<br /> ' +
            'Font size: ' +
            'Medium'
        );

      $('#edit-font-family').change(function () {
        // let theme_color = $(this).parents('#edit-rigel').find('#edit-theme-color').children('option:selected').text();
        // let font_size = $(this).parents('#edit-rigel').find('#edit-font-size').children('option:selected').text();
        let font_family_value = $(this).children('option:selected').text();

        if (font_family_value === '' || font_family_value === null) {
          font_family_value = 'Lucida Grande';
        }
        // $('li a[href="#edit-rigel"]').find('strong').siblings('span').html('Theme: ' + theme_color +'<br /> ' + 'Font family: ' + font_family_value + '<br /> ' + 'Font size: ' + font_size);

        $('#edit-rigel')
          .find('strong')
          .siblings('span')
          .html(
            'Theme: ' +
              'Dark' +
              '<br /> ' +
              'Font family: ' +
              font_family_value +
              '<br /> ' +
              'Font size: ' +
              'Medium'
          );
        // $('#edit-rigel').find('strong').siblings('span').html('Font family: ' + font_family_value + '<br /> ');
      });

      // Change body font family.
      let body_font_family = $('body').css('font-family');
      let replace_body_font_family = ' ' + body_font_family.replace(/\"/g, '');
      let font_family_array = replace_body_font_family.split(',');

      if (font_family !== '') {
        // Get index of selected font family.
        let index = font_family_array.indexOf(' ' + font_family);
        if (index !== '') {
          [font_family_array[0], font_family_array[index]] = [
            font_family_array[index],
            font_family_array[0]
          ];
        }
      }
      // alert(font_family_array);
      // Get updated font family.
      let updated_font_family = font_family_array.toString();
      // alert(updated_font_family);
      if (updated_font_family !== '') {
        $('body').css('font-family', updated_font_family);
      }
    }
  };

  // Initialize modal behavior.
  Drupal.behaviors.videoModal = {
    attach: function () {
      let video_src;
      $('.video').click(function () {
        video_src = $(this).attr('data-src');
      });

      // Play the video.
      $('#modal').on('shown.bs.modal', function (e) {
        $('#video').attr('src', video_src);
      });

      // Unset the video.
      $('#modal').on('hide.bs.modal', function (e) {
        $('#video').attr('src', '');
      });
    }
  };
})(jQuery);
