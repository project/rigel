/**
 * @file
 * Provides an event handler for hidden elements in dropdown menus.
 * Do not need this file anymore.  This is solved using CSS.
 */

(function ($) {
  // Dropdown menu //
  $(document).on('click', '.custom-dropdown > button.uparrow', function () {
    $(this).parents().find('.table-responsive').css('overflow', 'inherit');

    $('.custom-dropdown .dropdown-menu li').each(function () {
      var $icon = $(this).find('span').clone();
      $(this).find('span').remove();
      $(this).find('a').prepend($icon);
    });
  });
})(jQuery);
