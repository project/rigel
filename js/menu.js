/**
 * @file
 * Functions for supporting tertiary navigation.
 */
(function ($, Drupal, once) {
  'use strict';
  Drupal.behaviors.menuBehavior = {
    attach: function (context) {
      // Added once() to the DOM selector because this can fire more than once.
      $(once('navbar-id', '.navbar a.dropdown-toggle', context)).on(
        'click',
        function (e) {
          let $el = $(this);
          let $parent = $(this).offsetParent('.dropdown-menu');
          $(this).parent('li').toggleClass('open');

          if (!$parent.parent().hasClass('nav')) {
            $el
              .next()
              .css({ top: $el[0].offsetTop, left: $parent.outerWidth() - 4 });
          }
          $('.nav li.open').not($(this).parents('li')).removeClass('open');
          return false;
        }
      );

      // Mobile Menu //
      // Added once() to the DOM selector because this can fire more than once.
      $(once('nav-toggler-id', '.navbar-toggler', context)).on(
        'click',
        function (e) {
          $('body').toggleClass('showbar-open');
          $('.navbar-toggler').toggleClass('open');
          $('#navbar-collapse')
            .toggleClass('active')
            .removeClass('show collapse');
          $('.wrapper').toggleClass('showbar-overlay').toggleClass('active');
        }
      );

      $(document).click(function (e) {
        var target = e.target;
        if (
          !$(target).is('.navbar a.dropdown-toggle, .navbar .navbar-toggler') &&
          !$(target)
            .parents()
            .is('.navbar a.dropdown-toggle, .navbar .navbar-toggler')
        ) {
          $('.navbar .navbar-collapse').removeClass('active');
          $('body').removeClass('showbar-open');
          $('.navbar-toggler').removeClass('open');
          $('.wrapper').removeClass('showbar-overlay active');
        }
      });

      var path = window.location.pathname;
      path = path.slice(0, path.lastIndexOf('/'));
      var base_url = window.location.origin;
      var path2 = base_url + path;

      $('.navbar-nav li a').each(function () {
        var href = $(this).attr('href');
        var finalurl = base_url + href;
        var geturl = finalurl.slice(0, finalurl.lastIndexOf('/'));
        if (path2 == geturl) {
          $(this).parents().addClass('open');
          if (window.location.href.indexOf('design') > -1) {
            return false;
          }

          $(this).parents().addClass('open');
          $(this).addClass('is-active');
          return false;
        }
      });
    }
  };
})(jQuery, Drupal, once);
