/**
 * Provide Bootstrap navbar preview.
 */
/**
 * @file
 * Functions for supporting tertiary navigation.
 */
(function ($, Drupal, once) {
  'use strict';

  Drupal.behaviors.bootstrapNavbarPreview = {
    attach: function (context) {
      var $context = $(context);
      var $preview = $context.find('#edit-navbar');
      $(once('navbar', $preview, context)).each(function () {
        var $body = $context.find('body');
        var $navbar = $context.find('#navbar .navbar');
        $preview
          .find('select[name="navbar_position"]')
          .bind('change', function () {
            var $position = $(this).find(':selected').val();
            $navbar.removeClass(
              'navbar-fixed-bottom navbar-fixed-top navbar-static-top container'
            );
            if ($position.length) {
              $navbar.addClass('navbar-' + $position);
            } else {
              $navbar.addClass('container');
            }

            // Apply appropriate classes to body.
            $body.removeClass(
              'navbar-is-fixed-top navbar-is-fixed-bottom navbar-is-static-top'
            );
            switch ($position) {
              case 'fixed-top':
                $body.addClass('navbar-is-fixed-top');
                break;

              case 'fixed-bottom':
                $body.addClass('navbar-is-fixed-bottom');
                break;

              case 'static-top':
                $body.addClass('navbar-is-static-top');
                break;
            }
          });
        $preview
          .find('input[name="navbar_inverse"]')
          .bind('change', function () {
            $navbar.toggleClass('navbar-inverse navbar-default');
          });
      });

      // Header Menu Section move //
      $('nav[aria-labelledby="block-rigel-account-menu-menu"]').prependTo(
        '.right-sec-wrapper'
      );
      $('.breadcrumb').wrap('<div class="breadcrumb-wrapper" />');
      $('.breadcrumb-wrapper').insertBefore(
        'nav[aria-labelledby="block-rigel-account-menu-menu"]'
      );
      $(
        '.breadcrumb-wrapper, nav[aria-labelledby="block-rigel-account-menu-menu"]'
      ).wrapAll('<div class="nav-top" />');

      if ($(window).width() < 992) {
        $('.nav-top').detach().appendTo('.navbar-header');
      }
      $(window).resize(function () {
        if ($(window).width() < 992) {
          $('.nav-top').detach().appendTo('.navbar-header');
        }
      });

      // Nav tabs //
      $('.nav-tabs li a').addClass('ripple-effect');

      // Tab Scroll //
      // Commented out until we use it.
      // $('#tab-scroll').slick({
      //   slidesToShow: 10,
      //   slidesToScroll: 3,
      //   variableWidth: true,
      //   autoplay: false,
      //   infinite: false,
      //   dots: false,
      //   responsive: [
      //     {
      //       breakpoint: 767,
      //       settings: {
      //         slidesToShow: 3,
      //         slidesToScroll: 2,
      //         adaptiveHeight: true,
      //       },
      //     }
      //   ],
      // });

      $('table th.is-active a[href*="sort=desc"]').each(function () {
        $('table th.is-active a').append(
          '<span class="caret downarrow"></span>'
        );
      });
      $('table th.is-active a[href*="sort=asc"]').each(function () {
        $('table th.is-active a').append('<span class="caret"></span>');
      });

      // Apply button in arrow //
      $(
        once(
          'form_actions',
          'form .form-actions input[value="Apply to selected items"]',
          context
        )
      ).each(function () {
        $(this).wrap(
          '<div class="btn btn-primary btn-wrap apply-btn ripple-effect" />'
        );
      });
      $(once('apply_btn', 'form .form-actions .apply-btn', context)).each(
        function () {
          $(this).prepend('<span class="glyphicon glyphicon-ok"></span>');
        }
      );
      $('form .form-date, form .form-time').addClass('form-control');

      [
        'k8s-namespace-cost-comparison',
        'k8s-project-cost-comparison',
        'k8s-namespace-costs',
        'k8s-project-costs'
      ].forEach(function (id) {
        $(
          once('form_submit', `#${id} .details-wrapper .form-submit`, context)
        ).each(function () {
          $(this).wrap(
            '<div class="btn btn-primary btn-wrap apply-btn ripple-effect" />'
          );
        });

        $(
          once('k8s_apply_btn', `#${id} .details-wrapper .apply-btn`, context)
        ).each(function () {
          $(this).prepend('<span class="glyphicon glyphicon-ok"></span>');
        });
      });

      // Alert Message section move */
      $('.alert.alert-success').prependTo('.main-container');
      $('.alert.alert-danger').prependTo('.main-container');
      $('.alert.alert-warning').prependTo('.main-container');
      $('button.close').on('click', function () {
        $(this).parent().hide();
      });

      // Tab js //
      $('.tabbable-panel ul.nav-tabs a').click(function () {
        // Check for active
        $('.tabbable-panel ul.nav-tabs li').removeClass('active');
        $(this).parent().addClass('active');

        // Display active tab
        let currentTab = $(this).attr('href');
        $('.tabbable-panel .tab-content div.tab-pane').hide();
        $(currentTab).show();

        return false;
      });

      // Min & Max input //
      $(".form-wrapper input[type='number']").each(function () {
        $(this).wrapAll('<span class="min-max-wrap" />');
      });

      // Nav Tabs //
      const tabWrapItem = document.getElementById('tab_wrap');
      const rightButtonItem = document.getElementById('right-button');
      if (tabWrapItem && rightButtonItem) {
        var scrlWidth = tabWrapItem.scrollWidth;
        var elemWidth = tabWrapItem.clientWidth;

        if (elemWidth < scrlWidth) {
          rightButtonItem.classList.remove('d-none');
          $('.tabs-wrap').toggleClass('wrapflex');
          rightButtonItem.classList.toggle('arrowdown');
        }
      }

      $(once('right-button', '#right-button', context)).click(function () {
        $('.tabs-wrap').toggleClass('wrapflex');
        $(this).toggleClass('arrowdown');
      });

      // Tooltip //
      var tooltipTriggerList = [].slice.call(
        document.querySelectorAll('[data-bs-toggle="tooltip"]')
      );
      var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl);
      });
    }
  };
})(jQuery, Drupal, once);
